package ex_pattern_observer_solution;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class SecondFollower implements Observer {
    
    static ArrayList<String> notificationsList = new ArrayList<String>();

    @Override
    public void update(Observable o, Object arg) {
        // TODO Auto-generated method stub
        notificationsList.add((String)arg);
        System.out.println("Current notifications from Second Follower :");
        for(String s:notificationsList) {
            System.out.println(s);
        }
    }
}