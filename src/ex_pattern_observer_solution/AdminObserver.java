package ex_pattern_observer_solution;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class AdminObserver implements Observer {
    
    static int nbFollowers = 0;
    static int nbNotifications = 0;
    ArrayList<String> listNotifications = new ArrayList<String>();

    @Override
    public void update(Observable o, Object arg) {
        // TODO Auto-generated method stub
        
        nbFollowers = o.countObservers();
        
        if(!listNotifications.contains(arg)) {
            listNotifications.add((String)arg);
            nbNotifications++;
        }
        
        System.out.println("Current number of followers : "+nbFollowers);
        System.out.println("Current number of notifications : "+nbNotifications);
    }

}
