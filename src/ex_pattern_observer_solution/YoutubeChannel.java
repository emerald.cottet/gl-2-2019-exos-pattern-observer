package ex_pattern_observer_solution;

import java.util.Observable;

public class YoutubeChannel extends Observable {
    void news() {
        String[] news = {"New video", "New vlog", "New comment"};
        for(String s: news) {
            setChanged();
            notifyObservers(s);
        }
    }
}