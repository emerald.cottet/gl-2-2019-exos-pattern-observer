package ex_pattern_observer_solution;

import java.util.Observable;
import java.util.Observer;

public class FirstFollower implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        // TODO Auto-generated method stub
        System.out.println("First Follower got the news : "+(String)arg);
    }
}